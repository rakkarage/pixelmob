# **PixelMob** 👹

<img align="right" src="icon.png">

Pixel-perfect animated slime monster.

- 📦 <http://henrysoftware.itch.io/godot-pixel-mob>
- 🌐 <http://rakkarage.github.io/PixelMob>
- 📃 <http://guthub.com/rakkarage/PixelMob>

[![.github/workflows/compress.yml](https://github.com/rakkarage/PixelMob/actions/workflows/compress.yml/badge.svg)](https://github.com/rakkarage/PixelMob/actions/workflows/compress.yml)
[![.github/workflows/deploy.yml](https://github.com/rakkarage/PixelMob/actions/workflows/deploy.yml/badge.svg)](https://github.com/rakkarage/PixelMob/actions/workflows/deploy.yml)
